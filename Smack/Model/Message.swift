//
//  Message.swift
//  Smack
//
//  Created by Kouame NDri on 11/24/17.
//  Copyright © 2017 Klassic Ventures. All rights reserved.
//

import Foundation
import Foundation

struct Message {
    public private(set) var message: String!
    public private(set) var userName: String!
    public private(set) var channelId: String!
    public private(set) var userAvatar: String!
    public private(set) var userAvatarColor: String!
    public private(set) var id: String!
    public private(set) var timeStamp: String!
}
