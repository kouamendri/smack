//
//  ProfileVC.swift
//  Smack
//
//  Created by Kouame NDri on 11/20/17.
//  Copyright © 2017 Klassic Ventures. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    // Outlets
    
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

      
    }

    @IBAction func closedModalPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func logoutPressed(_ sender: Any) {
        UserDataService.instance.logoutUser()
        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func setUpView() {
         userName.text = UserDataService.instance.name
         userEmail.text = UserDataService.instance.email
        profileImg.image = UIImage(named: UserDataService.instance.avatarName)
        profileImg.backgroundColor = UserDataService.instance.returnUIColor(components: UserDataService.instance.avatarColor)
        
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.closeTap(_reconziger:)))
        bgView.addGestureRecognizer(closeTouch)
        
        
    }
    
    @objc func closeTap(_reconziger: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
}
