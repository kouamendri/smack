//
//  RoundedView.swift
//  Smack
//
//  Created by Kouame NDri on 11/20/17.
//  Copyright © 2017 Klassic Ventures. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedView: UIView {
    
    
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }


    override func awakeFromNib() {
        
    }
    
    func setUpView() {
        self.layer.cornerRadius = cornerRadius
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setUpView()
    }


}
